/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.robotbombnmap;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class Test_Robot {
        public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Map tableMap = new Map(10, 10);
        Robot robot = new Robot('N', 2, 2, tableMap);
        Bomb bomb = new Bomb(5, 5);
        tableMap.setRobot(robot);
        tableMap.setBomb(bomb);
        while (true) {
            tableMap.showMap();
            char direction = inputDirection(kb);
            if(direction == 'q'){
                System.out.println("Bye bye");
                break;
    
            }
            robot.walk(direction);
        }

    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }

}
