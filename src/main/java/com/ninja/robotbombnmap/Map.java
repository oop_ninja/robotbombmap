/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.robotbombnmap;

/**
 *
 * @author USER
 */
public class Map {
        private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public Map(int width, int height, Robot robot, Bomb bomb) {
        this.width = width;
        this.height = height;
        this.robot = robot;
        this.bomb = bomb;

    }
       public Map(int height, int width) {
        this.width = width;
        this.height = height;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    

}
public void showMap() {
        System.out.println("Map");
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (robot.isOn(x, y)) {
                    showRobot();
                } else if (bomb.isOn(x, y)) {
                    showBomb();
                } else {
                    System.out.print("-");
                }
            }
            System.out.println("");
        }
    }
        
    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
}

